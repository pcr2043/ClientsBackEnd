<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserAuthentication extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
       $response = $this->json('POST', '/api/login', ['email' => 'admin@clients.app', 'password' => 'secret']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'result' => true,
            ]);
    }
}
