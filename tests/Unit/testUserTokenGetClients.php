<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class testUserTokenGetClients extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
    	$response = $this->json('POST', '/api/login', ['email' => 'admin@clients.app', 'password' => 'secret']);

    	$token = $response->original['user']['api_token'];
    	
    	$response = $this->withHeaders([
    		'Authorization' => 'Bearer '.$token,
    	])->json('GET', '/api/clients');

    	$response
    	->assertStatus(200)
    	->assertSeeText('data');
    }
}
