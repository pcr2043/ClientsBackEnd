<?php

namespace App\Clients;

use App\Client;
use App\Http\Resources\ClientsCollection;

class Clients
{

	public function all()
	{
		return new ClientsCollection(Client::paginate());
	}

	public function save()
	{
		$rules =[
			'first_name' => 'required|max:50',
			'last_name' => 'required|max:50',
			'email' => 'required|unique:clients'
		];

		if(request()->id > 0)
			$rules['email'] ='required|unique:clients,email,'.request()->id.',id';

		$clientData = request()->validate($rules);

		try
		{
		//create instance of client
			$client = new Client();

			if(request()->id > 0)
				$client = Client::find(request()->id);


			$client->fill($clientData);
			$client->save();

			return ['result' => true, 'model' => $client];
		}
		catch(\Exception $ex)
		{
			return ['result' => false, 'msg' => $ex->getMessage()];
		}
		
	}

	public function delete(Client $client)
	{
		try
		{
			$client->delete();
			return ['result' => true];
		}
		catch(\Exception $ex)
		{
			return ['result' => false, 'msg' => $ex->getMessage()];
		}
	}

}