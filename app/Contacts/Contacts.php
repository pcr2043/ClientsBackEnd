<?php

namespace App\Contacts;

use App\Client;
use App\Contact;
use App\Http\Resources\ContactsCollection;

class Contacts
{


	public function all(){

		return Contact::all();
	}

	public function clientContacts(Client $client)
	{
		return new ContactsCollection($client->Contacts()->paginate());
	}

	public function save()
	{
		$rules =[
			'address' => 'required|max:100',
			'city' => 'required|max:100',
			'postcode' => 'required|max:20',
			'country_code' => 'required|max:100',
			'client_id' => 'required'
		];

	
		$clientData = request()->validate($rules);

		try
		{
			
			//create instance of contact
			$contact = new Contact();

			if(request()->id > 0)
				$contact = Contact::find(request()->id);


			$contact->fill($clientData);
			$contact->save();

			return ['result' => true, 'model' => $contact];
		}
		catch(\Exception $ex)
		{
			return ['result' => false, 'msg' => $ex->getMessage()];
		}
		
	}

	public function delete(Contact $contact)
	{
		try
		{
			$contact->delete();
			return ['result' => true];
		}
		catch(\Exception $ex)
		{
			return ['result' => false, 'msg' => $ex->getMessage()];
		}
	}
}
