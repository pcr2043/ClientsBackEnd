<?php

namespace App;

use App\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
	use SoftDeletes;

	protected $table='clients_contacts';	

    protected $fillable =[];

    protected $guarded =['id'];

    protected $perPage = 3;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function Client(){
    	
    	return $this->belongTo(Client::class);
    }

}
