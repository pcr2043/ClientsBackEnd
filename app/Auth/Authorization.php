<?php

namespace App\Auth;

class Authorization
{


	public function login()
	{
		$credentials =	 request()->validate([
			'email' => 'required|exists:users',
			'password' => 'required'
		]);	

		if(auth()->guard('web')->attempt($credentials, true))
			return ['result' => true, 'user'=> auth()->user()];
		else
			return ['result' => false, 'msg' =>'Bad Credentials'];
	}

}

