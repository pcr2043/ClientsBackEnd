<?php

namespace App\Http\Controllers;

use App\Auth\Authorization;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $auth;

    public function __construct(Authorization $auth)
    {
    	$this->auth = $auth;
    }

    public function login(){

    	return $this->auth->login();
    }
}
