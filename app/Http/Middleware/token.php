<?php

namespace App\Http\Middleware;

use Closure;

class token
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        if (!auth()->guard('api')->user()) {

            return response()->json(['result' => false, 'error' => "Not Authorized!!"], 403);

        }


        return $next($request);
    }
}
