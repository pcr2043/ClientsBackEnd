<?php

namespace App;

use App\Contact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $appends =['name'];

    protected $fillable =[];

    protected $guarded =['id'];

    protected $perPage = 10;

    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function Contacts(){

    	return $this->hasMany(Contact::class);
    }
}
