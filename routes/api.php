<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//some header to by pass cross


Route::post('login', 'AuthController@login');
Route::resource('clients', 'ClientsController');
Route::get('contacts/{client}', 'ContactsController@clientContacts');
Route::resource('contacts', 'ContactsController');


