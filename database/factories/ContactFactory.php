<?php

use App\Client;
use Faker\Generator as Faker;

$factory->define(App\Contact::class, function (Faker $faker) {

	return [
		'address' => $faker->address,
		'city' => $faker->city,
		'postcode' => $faker->postcode,
		'country_code' => $faker->countryCode,
		'client_id' => Client::find(mt_rand(1,20)),
	];

});
