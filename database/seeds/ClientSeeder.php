<?php

use App\Client;
use App\Contact;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = factory(Client::class, 200)->create();

        foreach($clients as $client)
        	factory(Contact::class, mt_rand(1, 10))->create(['client_id' => $client->id]);
    }
}
