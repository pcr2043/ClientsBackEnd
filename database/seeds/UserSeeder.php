<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$user = new User;
    	$user->name = "Administrator";
    	$user->email = "admin@clients.app";
    	$user->password = bcrypt('secret');
    	$user->save();

    	//create unique token
    	$user->api_token = str_random(10).$user->id;
    	$user->save();
    	
    }
}
