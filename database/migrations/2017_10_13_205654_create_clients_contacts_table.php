<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients_contacts', function (Blueprint $table) {

          $table->increments('id');
          $table->string('address');
          $table->string('city');
          $table->string('postcode');
          $table->string('country_code');
          $table->integer('client_id')->unsigned()->length(10);

          $table->softDeletes();

          //FOREIGN KEYS
          echo "Adding Client Foreign Key \n";
          $table->foreign('client_id', 'ref_client')->references('id')->on('clients')->onDelete('cascade');


          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients_contacts');
    }
}
